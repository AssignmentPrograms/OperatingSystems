

#include "common.h"

#include <semaphore.h>
#include <pthread.h>

#include <stdio.h>
#include <stdlib.h>


/*
bufferSize
mutexLock, empty, full
*/

#define SLEEPTIME 60

#define NUM_THREADS 10


#define FIFO  SCHED_FIFO
#define RR    SCHED_RR
#define OTHER SCHED_OTHER

#define SCHED_TYPE FIFO

static inline void InitializeBuffer()
{
    // Initialize all buffer items to zero
    for( int i = 0; i < bufferSize; i++ )
    {
        buffer[i] = 0;
    }
}


int main( int argc, char** argv )
{
    int sleepTime = SLEEPTIME;
    int numProducers = NUM_THREADS;
    int numConsumers = NUM_THREADS;
    
    
    
    if( argc != 1 && argc != 4 )
    {
        printf("Expected 3 arguments or no arguments!\n");
        printf("%i arguments given!\n", argc - 1);
        exit(1);
    }
    else if( argc == 4 )
    {
        //printf("Valid arguments given!\n");
        sleepTime = atoi(argv[1]);
        numProducers = atoi(argv[2]);
        numConsumers = atoi(argv[3]);
    }
    else
    {
        printf("Using default values!\n");
    }
    
    printf("Parameters are set to the following:\n");
    printf("Sleep Time: %i\n", sleepTime);
    printf("Number of Producer Threads: %i\n", numProducers);
    printf("Number of Consumer Threads: %i\n", numConsumers);
    
    pthread_t producerThread[numProducers];
    pthread_t consumerThread[numConsumers];
    
    
    // Initialize our semaphores
    sem_init(&empty, 1, bufferSize);
    sem_init(&full, 1, 0);
    
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    
    InitializeBuffer();
    
    // Initialize our mutex lock
    pthread_mutex_init(&mutexLock, 0); // 0 = Default atr
    
    int error;
    
    
    // SCHED_FIFO, SCHED_RR, or SCHED_OTHER
    switch( SCHED_TYPE )
    {
        case FIFO:
        {
            if( pthread_attr_setschedpolicy( &attr, SCHED_FIFO ) )
                printf("Unable to set scheduler policy to FIFO\n");
        } break;
        
        case RR:
        {
            if( pthread_attr_setschedpolicy( &attr, SCHED_RR ) )
                printf("Unable to set scheduler policy to RR\n");
        } break;
        
        default: // OTHER
        {
            if( pthread_attr_setschedpolicy( &attr, SCHED_OTHER ) )
                printf("Unable to set scheduler policy to OTHER\n");
        } break;
    }
    
    int sched_policy = 0;
    pthread_attr_getschedpolicy( &attr, &sched_policy );
    
    switch( sched_policy )
    {
        case FIFO:
        {
            printf("Current scheduler policy is SCHED_FIFO\n");
        } break;
        
        case RR:
        {
            printf("Current scheduler policy is SCHED_RR\n");
        } break;
        
        case OTHER: // OTHER
        {
            printf("Current scheduler policy is SCHED_OTHER\n");
        } break;
        
        default:
        {
            printf("Current scheduler policy is invalid!\n");
        }
    }
    
    
    
    // NOTE: With out multiple consumer threads the consumer gets rather behind. But with 10 consumer threads the consumer never gets far behind... COOL!
    
    for( int i = 0; i < NUM_THREADS; i++ )
    {
        if( ( error = pthread_create(&producerThread[i], 0, (void*)(*Producer), 0) ) > 0 )
        {
            printf("Error! %i\n", error);
            exit(1);
        }
        if( ( error = pthread_create(&consumerThread[i], 0, (void*)(*Consumer), 0) ) > 0 )
        {
            printf("Error! %i\n", error);
            exit(1);
        }
    }
    
    sleep(sleepTime);
    
    pthread_mutex_destroy(&mutexLock);
    
    return 0;
}