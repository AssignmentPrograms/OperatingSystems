

#ifndef COMMON_H
#define COMMON_H

#include <semaphore.h>
#include <pthread.h>

#include <stdio.h>
#include <unistd.h> // Contains sleep( seconds )
#include <stdlib.h> // Contains rand()

#define BUFFER_SIZE 5

#define MAXSLEEP 10

// Was used to exit and join threads, but use would require another
//critical section handler
//int running = 1;

// Semaphore Data
sem_t empty;
sem_t full;

pthread_mutex_t mutexLock;


// Buffer Data
int bufferSize = BUFFER_SIZE;

int buffer[BUFFER_SIZE];
int in = 0; // Represents next spot to add item to
int out = 0; // Represents next spot to take item from

// Just going to use a buffer of chronological 1-15 for debugging convenience
int item;

typedef int buffer_item;

static inline int insert_item(buffer_item item)
{
    if( in > bufferSize && in < 0 )
        return -1;
    
    // Code to add item to the buffer
    buffer[in] = item;
    in = (in + 1) % bufferSize;
    
    return 0;
}


void Producer()
{
    int sleepTime;
    int rv; // Return value of producing call
    
    while(1)
    {
        sleepTime = rand() % MAXSLEEP;
        sleep(sleepTime);
        
        // Ensuring that it is withing range of integer
        item = rand() % 100000;
        
        
        sem_wait(&empty); // wait()
        pthread_mutex_lock(&mutexLock);
        // NOTE: You are now entering Critical Section
        
        rv = insert_item(item);
        
        // NOTE: You are now leaving Critical Section
        pthread_mutex_unlock(&mutexLock);
        sem_post(&full); // signal()
        
        if( rv )
            printf("Error producing %i\n", item);
        else
            printf("Producer produced: %i\n", item);
    }
    //printf("OUTprod");
    //pthread_exit(0);
}

static inline int remove_item(buffer_item* item)
{
    if( out > bufferSize && out < 0 )
        return -1;
    
    // Code to remove item from buffer
    *item = buffer[out];
    out = (out + 1) % bufferSize;
    
    return 0;
}

void Consumer()
{
    int sleepTime;
    int rv; // Return value of consuming call
    
    while(1)
    {
        sleepTime = rand() % MAXSLEEP;
        sleep(sleepTime);
        
        
        sem_wait(&full); // wait()
        pthread_mutex_lock(&mutexLock);
        // NOTE: You are now entering Critical Section
        
        rv = remove_item(&item);
        
        // NOTE: You are now leaving Critical Section
        pthread_mutex_unlock(&mutexLock);
        sem_post(&empty); // signal()
        
        if( rv )
            printf("Error consuming %i\n", item);
        else
            printf("Consumer consumed: %i\n", item);
    }
    //printf("OUTcon");
    //pthread_exit(0);
}

#endif //COMMON_H