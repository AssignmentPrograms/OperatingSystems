#include <stdio.h>
#include <stdlib.h>

int main( int argc, char** argv )
{
    if( argc != 2 )
    {
        printf("No command line argument (I expect an address)!\n");
        exit(0);
    }
    
    int address = atoi( argv[1] );
    // We want to disregard the low order 12 bits
    // 12 bits because 4KB page size, so need 12 bits to represent al values
    int pageNum = address >> 12;
    // We want to keep only the low order 12 bits
    int offset = address & 0xfff;
    
    printf( "The address %i contains:\n", address );
    printf( "page number = %i\n", pageNum );
    printf( "offset = %i\n",offset );
    
    
    return 0;
}